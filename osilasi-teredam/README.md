## Osilasi Harmonik Teredam 

Osilator harmonik teredam merupakan masalah klasik dalam mekanika. Ini menggambarkan pergerakan osilator mekanis (misalnya pendulum pegas) di bawah pengaruh gaya pemulih dan gesekan.


![Skema Osilasi hermonik teredam](https://gitlab.com/marchgis/march-ed/2023/courses/70301020-gelombang/-/raw/main/osilasi-teredam/Osilasi1.png) 
![Pegas Teredam](https://gitlab.com/marchgis/march-ed/2023/courses/70301020-gelombang/-/raw/main/osilasi-teredam/Damped_spring.gif) 


## Persamaan Gerak 
Persamaan gerak adalah persamaan matematika yang sepenuhnya menggambarkan perkembangan spasial dan temporal dari sistem dinamis di bawah pengaruh gaya eksternal. Sebagai aturan, ini adalah persamaan diferensial orde kedua. Pada bagian ini, pertama-tama kita mempertimbangkan gaya-gaya yang mempengaruhi pergerakan pendulum.

Gerakan pendulum bergantung pada keseimbangan tiga gaya: Inersia, gaya pemulih pegas dan gesekan.

| header | header |
| ------ | ------ |
| ![Pegas Teredam](https://gitlab.com/marchgis/march-ed/2023/courses/70301020-gelombang/-/raw/main/osilasi-teredam/Osislasi2.png)| $a^2$

Hukum kedua Newton menyatakan bahwa setiap perubahan dalam gerak suatu benda sebanding dengan gaya yang bekerja padanya dan bahwa perubahan dalam gerak ini selalu terjadi dalam arah yang sama dengan gaya yang bekerja.

Pada pendulum memiliki gaya  $F_f$ yang berlaku saat berosiasi. Ini menarik massa pendulum kembali ke posisi istirahatnya. Gaya ini berbanding lurus dengan defleksi pendulum. Arahnya berlawanan dengan arah defleksi pendulum. Kekuatannya dihitung dengan mengalikan jarak defleksi dengan konstanta pegas yang bergantung pada material $k$.

pendulum akan mengalami gesekan. Gaya gesekan $F_R$ berbanding lurus dengan kecepatan pendulum. Arahnya berlawanan dengan arah grak pendulum. parameter $\mu$ koofisein gesek yang benrgantu pada bahan dan bentuk (lihat Hukum Stokes). Oleh karena itu gaya inersia ditentang oleh dua gaya sebagai berikut : 

$$m \ddot{x}=-\mu \dot{x} - kx$$ 

kita dapat mengubahnya menjadi persamaan berikut : 

$$\ddot{x}+{\mu \over m}\dot{x}+{k \over m}x=0 \tag{1} $$

Persamaan ini adalah persamaan diferensial orde dua linier homogen dengan koefisien konstan. Menentukan jenis persamaan diferensial penting karena pendekatan yang diperlukan untuk menyelesaikannya bergantung padanya. Ansatz eksponensial biasanya dipilih untuk menyelesaikan persamaan diferensial jenis ini.

## Ansatz eksponensial

Persamaan \ref{1} adalah persamaan diferensial linier homogen orde kedua dengan koefisien konstan. Asumsinya adalah bahwa solusi persamaan diferensial tersebut adalah fungsi eksponensial. Metode ini pertama kali dijelaskan oleh ahli matematika Jerman Leonard Euler. Oleh karena itu disebut "ansatz eksponensial".

Ansatz eksponensial menyatakan bahwa solusi khusus untuk persamaan diferensial berbentuk:

$$x(t) = Ce^{\lambda t}$$ \tag{2}

dengan membentuk turunan pertama dan kedua menggunakan persamaan \tag{2}, kita akan mendapatkan :

$$\dot{x}(t) = \lambda Ce^{\lambda t} \ddot{x}(t) = \lambda^2 Ce^{\lambda t} \tag{3}$$

denga memasukan persamaan \tag{2} dan persamaan \tag{3} kedalam persamaan \tag{1} kita akan mendapatkan : 

$$\lambda^2 Ce^{\lambda t} + {\mu \over m} \lambda Ce^{\lambda t} + {k \over m} Ce^{\lambda t} = 0 $$

sederhanakan dengan membagi dengan $Ce^{\lambda t}$, sehingga : 

$$ \lambda^2+{\mu \over m} \lambda + {k \over m} =0 \tag{4}$$

Persamaan ini disebut juga persamaan karakteristik. Ini adalah persamaan kuadrat dalam bentuk normal. Oleh karena itu ada dua solusi berikut untuk $\lambda$ : 

$$ \lambda_{1,2}=-{\mu \over 2m } \plusmn \sqrt{\Big({\mu \over 2m}^2-{k \over m}} \tag{5}$$

Untuk penyederhanaan lebih lanjut kami memperkenalkan konstanta baru $\delta$ dan $\omega_0$

sehingga persamaan $\tag{5}$ ditulis sebagai : 

$$\lambda_{1,2}=-\delta \plusmn \sqrt{\delta^2-\omega_{0}^{2}} \tag{6}$$

Solusi lebih lanjut tergantung pada nilai $\lambda_1$ dan \lambda_2. Kita perlu mempertimbangkan beberapa kemungkinan yang berbeda, istilah di dalam akar kuadrat ini disembut diskriminan. nilai ini tergantung pada pilihan konstanta $\delta$ dan $\omega_0$. diskriminan bisa lebih besar dari nol atau sama dengan nol. Oleh karena itu $\lambda_1$ dan $\lambda_2$ dapat dianggap sebagai berikut : 

- Dua Solusi bernilai nyata yang berbeda satu sama lain 
- Dua solusi kompleks konjugat
- Dua solusi bernilai nyata yang identik.

Masing-masing dari ketiga opsi tersebut memerlukan pendekatan solusi yang berbeda. Solusi umum dari persamaan diferensial homogen memiliki bentuk sebagai berikut : 

$$ x(t) = C_{1x1}(t)+C_{2x2}(t) \tag{7}$$

fungsi $x1 (t)$ dan $x2(t)$ merupakan determinan yang ditentukan dari diskriminan pada persamaan $\tag{6}$. Mari kita lihat beberapa kemungkinan yang berbeda. 

## Kasus Overdamped

jika nilai $\delta \gt \omega_0$ kita kana mendapatkan kasus dimana gesekan yang akan kuat. oleh karena itu diskriminan dalam persamaan (6) adalah positif dan ada dua solusi bernilai nyata yang berde untuk $\lambda$. Solusi untuk persamaan diferensial ini adalah sebagai berikut : 

$$ x_1(t)=C_{1}e^{\lambda_1 t} x2(t)=C_{2}e^{\lambda_{2}t}$$

dengan memasukan nilai ini kedapam Persamaan (7), kita akan mendapatkan solusi umum dari persamaan diferensial sebagai berikut: 

$$ x(t) = C_{1}e^{\lambda 1 t} + C_{2}e^{\lambda 2 t} $$

kita akan mendapatkan persamaan (8) sebagai berikut setelah memasukan nilai $\lambda$ berdasarkan persamaan (6) : 

$$ x(t) = C_{1} e^{\delta + \sqrt{\delta^2)-\omega_{0}^{2}}} + C_{2}e^{-\delta-\sqrt{\delta^2 - \omega_0^2}t} \tag{8}$$

mari kita perhatikan eksponen pada persamaan (8). nilai ekspones seharusnya bernilai negativa karena untuk kasus overdamping, berikut akan bernilai benar : 

$$\delta \gt \sqrt{\delta^2 - \omega_{0}^{2}} \tag{9}$$

dimana persamaan(8) merupakan pertambahan fungsi peluruhan, untuk menyederhanakan persamaan kita akan subsitusi persamaan akar dengan konstanta yang baru :

$$\alpha = \sqrt{\delta^2 - \omega_0^2} \tag{10}$$

setlah memasukan konstanta yang baru pada persamaan (8) maka kita dapat menulis ulang kembali persmaan sebagai berikut : 

$$ x(t)=e^{-\delta t} \Big(C_{1}e^{\alpha t} + C_{2}e^{\alpha t} \Big) \tag{11}$$

Ini adalah solusi kasus overdampe, dimana nilai konstanta $C_{1} dan C_{2}$ untuk kasus ini dapat ditentukan dari kondisi awal yang diberikan. Misalkan posisi awal dan kecepatan awal dari pendulum dapat dberikan. 

$$ x(0) = x_0 , \dot{x}(0) = v_0 \tag{12} $$

## Kasus Teredam Kritis

Ketika kita membaha teredam kritis yaitu ketika nilai $\delta = \omega_0$. Hal ini merupak perpindanah dari overdamping terhadap osilasi. Pada kasus ini persmaan (6) memiliki satu solus untuk nilai $\lambda$ sebagi berikut : 

$$ \lambda_1=\lambda_2=\lambda=-\delta \tag{13}$$

menggunakan eksponen ansatz pada kasus ini menggunakan pendekatan untu mendapatkan dua solusi parsial untuk persamaan diferensial sebagai berikut : 

$$ x_1(t) = C_1 e^{\lambda t} x_2(t)= t C_2e^{\lambda t} \tag{14}$$

dengan memasukan persmaan (13) dan (14) pada solusi umum (7) solusi untuk kasus teredam kritis adalah : 

$$ x(t)= e^{-\delta t} \Big(C_1 +tC_2 \Big) \tag{15} $$

Integrasi konstanta $C_1$ dan $C_2$ harus diperoleh dari kondisi awal suatu masalah tertentu.

$$ x(0) = x_0,  \dot{x}(0)=v_0 $$

## Referensi 


1. "Der Exponentialansatz zur Lösung der linearen, homogenen Dgl. 2. Ordnung." Lukas Geiling, Martin Wagener (Dr. W. Seifert), Lehrdokument, Institute of Physics, University Halle-Wittenberg, D-06099 Halle, Germany
2. "Gedämpfte harmonische Schwingungen." Barbara Herzog, Kim Holm, Lehrdokument, Institute of Physics, University Halle-Wittenberg, D-06099 Halle, Germany
3. "Freie gedämpfte Schwingung", Online-Angebot der Georg-August-Universität Göttingen, Georg-August-Universität Göttingen
4. https://beltoforion.de/en/harmonic_oscillator/


