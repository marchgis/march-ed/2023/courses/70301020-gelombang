# Underdamp case
Underdamp osilasi terjadi ketika $\delta \lt \omega_0$ pada kasus ini persamaan diskriminan pada persamaan (6) ada negatif, karen itu $\lambda_1$ dan $\lambda_2$ ada bilangan kompleks.  $x(t) = Ce^{\lambda t}$ untuk memcahkan solusi ini menggaunakan persamaan diferensial 

$$ x1(t) = C_1 e^{\lambda_1 t}$$

$$ x2(t) = C_2 e^{\lambda_2 t}$$

dengan memasukakan eksponensial kepersamaan (7). dan mengga $\lambda$ dengan persamaan (6) kita akan mendapatkan :

$$ x(t) = e^{-\delta t} \Big(C1 e^{\sqrt{\delta^2 - \omega_o}} + C2 e^{-\sqrt{\delta^2 - \omega_o }t } \Big) \tag{17} $$

Kita telah melihat persamaan ini dalam bentuk yang sedikit dimodifikasi sebagai persamaan
(11) Ini adalah solusi umum untuk persamaan diferensial dari osilator harmonik. Kita sekarang berurusan dengan solusi bernilai kompleks karena dalam kasus underdamped suku di bawah akar kuadrat adalah negatif. Oleh karena itu konstanta $C1$ dan $C2$ adalah nilai-nilai yang kompleks. Untuk menangani bilangan kompleks kita menggunakan rumus Euler. Persamaan ini menetapkan hubungan antara fungsi eksponensial kompleks dan fungsi trigonometri:

$$ e^{i \phi} = cos \phi + i sin \phi $$

Sangat membantu untuk mengatur ulang persamaan(17) sedemikian rupa sehingga bagian imajinernya diisolasi:

$$ \sqrt{\delta^2 - \omega_0^2} = \sqrt{-1 * (\omega_0^2-\delta)} = i \sqrt{\omega_0^2 - \delta^2} \tag{18} $$

Kita memperoleh istilah yang terdiri dari unit imajiner $i$ dikalikan dengan akar kuadrat bernilai riil. Untuk menyederhanakan perhitungan lebih lanjut, kita mengganti akar kuadrat dengan nama konstanta baru $\omega$ : 

$$\omega = \sqrt{\omega_0^2-\delta^2} \tag{19}$$


$$x(t) = e^{-\delta t} \Big( \underbrace{C_1 e^{i \omega t} + C_2 e^{- i \omega t}}_{\hat{x}(t)}\Big) \tag {20}$$

$$ C_1 = \hat{C_1} e^{i \phi_1}$$
$$ C_2 = \hat{C_2} e^{i \phi_2} \tag{21} $$


![tes](osilasi-teredam/equation.svg)

Dalam persamaan(22) bagian bilangan nyata dan imajiner dari persamaan dipisahkan. kita hanya akan tertarik pada solusi yang bagian imajinernya menghilang. Hal ini terjadi jika hal berikut ini benar:

![tes1](osilasi-teredam/equation-2.svg)

dari persamaan(20)menghilang jika:
$$ \hat{C_1} = \hat{C_2}, \phi_1 = -\phi_2 \tag{24} $$

atau dengan kata lain : kontanta $C_1$ dan $C_2$ harus konjugasi kompleks: 

$$ C_1 = C_2^* $$ 

dengan memasukan persamaan (24) dengan mengingat persamaan (22) kita akan mendapatkan $\hat x(t)$ : 

![tes2](osilasi-teredam/equation-3.svg)

dengan menggunakan persamaan (25) pada persamaan (20) kita akan mendapatkan solusi persamaan diferential equation untuk kasus underdamps case sebagai berikut: 

$$ x(t) = e^{\delta t}\Big( 2A \cos (\phi + \omega t)\Big) \tag{26}$$

kontantan $\phi_1$ dan $\hat C_1$ kita kana mengganti dengan A dimana A adalah amplitudo dan $\phi$ merupakan sudut phase yang didaptkan dengan kondisi awal seperti : 

$$ x(0) = x_0, \dot x(0) = v_0 \tag{27}$$



