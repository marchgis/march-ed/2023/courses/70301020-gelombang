# Learning Objective

Pada akhir bagian ini, Anda akan dapat:

- Tentukan osilasi paksa
- Membuatdaftar persamaan gerak yang terkait dengan osilasi paksa
- Menjelaskan konsep resonansi dan pengaruhnya terhadap amplitudo osilator
- Menyebutkan ciri-ciri sistem yang berosilasi dalam resonansi
