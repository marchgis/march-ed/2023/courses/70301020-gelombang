# 70301020 - Gelombang

## Learning Outcome Outline
- Mampu memahami konsep osilasi dan jenis-jenis osilasi beserta persamaan penyelesaiannya.				
- Mampu mengaplikasikan persamaan gelombang pada fenomena fisika yang berhubungan dengan gelombang.				
- Mampu merumuskan problem fisika secara matematis.				
- Mampu belajar secara mandiri dengan efektif.								

## Pertemuan

No | Learning Outcome | Materi & Asesmen
---|---|---
1 | <ol><li>Mampu menjelaskan konsep dasar gelombang </li><li> Mampu menurunkan persamaan gelombang untuk beberapa fenomena Pegas(posisi, kecepatan, percepatan), konsep Energi pada bend yang berosilasi </li><li>Mampu menyelesaiakan problem fisika berkaitan dengan konsep osilasi harmonik</li></ol> | [Materi](https://docs.google.com/presentation/d/1kxAt5U2qTWWUUKarFImBzlIkQm3HSi0z-jIR2T8oPfY/edit?usp=share_link)
2 | <ol><li>Mampu menurunkan persamaan gelombang untuk beberapa fenomena Bandul matematis</li><li> Mampu menurunkan persamaan gelombang pada rangakaian listrik LC. </li><li>Mampu menyelesaiakan problem fisika berkaitan dengan konsep osilasi harmonik berkaitan dengan bandul matematis dan rangakain listrik LC </li></ol> | [Materi](https://docs.google.com/presentation/d/1kxAt5U2qTWWUUKarFImBzlIkQm3HSi0z-jIR2T8oPfY/edit?usp=share_link)
3 | Osilasi Teredam |[Materi](https://gitlab.com/marchgis/march-ed/2023/courses/70301020-gelombang)
4 | Underdamp Case|[Materi](https://gitlab.com/marchgis/march-ed/2023/courses/70301020-gelombang/-/blob/main/osilasi-teredam/underdamp-case.md)  
5 | Latihan Osilasi Teredam|[Materi]
6 | |
7 | |
 | UTS |
8 | |
9 | |
10 | |
12 | |
13 | |
14 | |
 | UAS |

## Tools


## Referensi
- [ H. J. Pain - The Physics of Vibrations and Waves, 6th Edition (2005, Wiley)](https://drive.google.com/file/d/1B3fpcqtsbk9sA2xdb14BydiM-wGH1dC4/view?usp=share_link)

- [Richard_Fitzpatrick_Oscillations_and_Waves_CRC_Press,_Taylor_&_Francis](https://drive.google.com/file/d/1dING8IjdQxbXp6nuNu9Ig8chjXUbyx5r/view?usp=share_link)


